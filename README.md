# Gitlab CI Workshop

This repository contains a tutorial to set up Gitlab and Gitlab CI on your
computer to play around with it.

## Overview

![overview diagram](img/overview.png)

We set up three containers on the host system

- *Gitlab* to provide the web interface, scm, etc.
- *Gitlab Runner* to allow running CI jobs
- *Docker Registry* for storing container images

## Requisites

* No reservation against containers
* A machine with reasonable amount of resources (8 GiB RAM, 30 GiB HD)
* A way of privilege escalation (e.g. `sudo`)

This tutorial is based on the usage of Docker container engine and Docker
Compose. For setup refer to [Docker Setup](#docker-setup).

## Initial Setup

The fist steps of installing and configuring the host system and the container
environment will be executed by running the following script:

```sh
./setup.sh
```

The steps taken by the script are also covered by the section
[Manual Setup](#manual-setup). After this initial setup, Gitlab is running on
the system and can be accessed in a web browser on port 5800.

The admin account `root` will be created with an initial password, which is
stored in `/etc/gitlab/initial_root_password` in the container. Display the
content of the file:

```shell
docker exec gitlab-ci-gitlab cat /etc/gitlab/initial_root_password
```

Since this is a local test environment, it isn't necessary to create a separate
user account. The root account is totally sufficient and allows system
configuration.

![admin menu](img/admin-menu.png)

In the **Menu | Admin** the following options should be changed:

- **Settings | CI/CD | Continuous Integration and Deployment**: deactivate
  *Auto-DevOps*
- In order to build and store containers, the variable `CI_REGISTRY` under
  **Settings | CI/CD | Variables** has to be set to `HOSTNAME:5000`. This
  will define the container registry for all CI jobs. The variable must not be
  set to *protected*.

In the account settings an SSH public key from the host system should be stored.
The following command will copy the public key `~/.ssh/id_ed25519.pub` into the
clipboard:

```sh
cat ~/.ssh/id_ed25519.pub | xsel -b
```

## Runner Registration

The *Runner* has to be registered on the Gitlab instance. The registration is
triggered by running the following command (inside the runner container):

```sh
docker exec -it gitlab-ci-gitlab-runner gitlab-runner register
```

In **☰ (Hamburger Menu) | Admin | CI/CD | Runners** is an overview of registered runners.
The button »Register an instance runner« reveals a token, which is needed for
the registration process. Note: the token is different for every registration.

First the instance URL and the token has to be entered. For the *Executor*
`shell` would simply execute all jobs on the runner directly, i.e. the runner
container. The runner container is, however, configured to have the system's
docker socket passed through. Therefore `docker` can be used as Executor. Tags
can be used to have certain jobs run on certain runners. Tag can be left empty.

After registration the runner should appear in the list (possibly site reload
required). A runner without tags will accept untagged jobs as soon as
registration is completed.

The runner configuration is located in the working directory of this git
repository under:

```
/runner-config/config.toml
```

This directory is excluded from source control via `.gitignore` file and can be
changed from the host system. Changes will apply immediately to the runner.

## First CI Pipeline

Create a new empty project. In the project overview select on the left sidebar
**CI/CD | Editor**. This will not only open an editor for the Gitlab CI
manifest, but also have an example configuration preloaded. For the first try we
take this example as is and save it to the repository with the button »Commit
changes«.

The pipeline should now be created and can be view in **CI/CD | Pipelines**. The
pipeline should be automatically picked up by the runner and be executed
sequentially.

For running job in parallel adjust the value of `concurrent` in `config.toml`.

## Runner Configuration for Docker

Since the runner itself (and the environment of CI pipeline jobs) run inside a
container, running and in particular building containers in not possible just
like that. The runner has to be configured, to pass the host systems docker
socket to the runner containers.

Edit the file `runner-config/config.toml` in this git repository and add
to volumes an entry for `/var/run/docker.sock` similar to the following:

```
[[runners]]
  [runners.docker]
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
```

It might be necessary, to also set `dns` explicitly for a setup with `dnsmasq`
(as described in [Docker — Name Resolution](#name-resolution)).

Now a job running a container should be possible. Here is a simple example:

```yaml
run container:
  image: docker
  script:
    - docker run --rm alpine ls -l /
```

For all options see section [Docker Executor🔗][runner-docker-executor] in the
Gitlab documentation.

## Common Errors

If building (or running) containers in an CI job fails with an error message
similar to the following, then the docker engine isn't properly configured to
run inside the runner.

```
dial tcp: lookup docker on 192.168.64.1:53: no such host
```

Make sure the docker socket is correctly passed to the runner (see
[Runner Configuration for Docker](#runner-configuration-for-docker)).

## Clean

After this workshop you can clean up and free up hard disk space.

Remove Containers using:

```shell
docker-compose down [-v]
```

With the `-v` switch volumes (containing configuration data, etc.) are removed
as well. Without removing volumes, all configuration, users, git repositories,
etc. are still preserved and `docker-compose up` should in theory restore a
working setup.

```shell
docker image prune -a
```

This will remove all locally stored container images.

---

## Docker Setup

On debian 11 all required components can be installed from the package
repository.

```shell
sudo apt install docker-compose
```

If not working as `root` user, the current user has to be member of `docker`
group.

```shell
sudo usermod -aG docker "$USER"
```

### Name Resolution

Docker containers usually apply the hosts DNS configuration. There is, however,
a caveat when caching DNS requests locally, i.e. by running a *dnsmasq* service.
When in doubt if you are using such a setup, have a look at your
`/etc/resolv.conf`, whether a line similar to the following is present!

```
nameserver 127.0.0.1
```

Docker will replace such configuration with `8.8.8.8`, which will prevent local
name resolution for the containers. The solution is, to have a second *dnsmasq*
service running, which will listen on the docker bridge device `docker0` and
forward DNS requests to the first service:

```sh
dnsmasq --server=127.0.0.1 --bind-interfaces \
  --listen-address=$(ip -4 addr show dev docker0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
```

## Manual Setup

The *Gitlab Runner* requires the configuration file to exist:

```sh
touch runner-config/config.toml
```

Host name is required:

```sh
echo HOST_NAME=$(hostname -f) >.env
```

### Starting

There are three containers defined in the compose file `docker-compose.yml`. To
have a look at the messages, each container produces, each container should be
started in its own terminal.

The *Gitlab* instance is started using:

```sh
docker-compose up gitlab
```

Then the *Gitlab CI Runner*:

```sh
docker-compose up gitlab-runner
```

And the *Docker Registry*:

```sh
docker-compose up registry
```

The whole system can be brought up by a single command:

```sh
docker-compose up -d
```

That will launch all containers at once in the background. In the
end `docker-compose ps` should give an output similar to this:

```
         Name                        Command                  State                                   Ports
----------------------------------------------------------------------------------------------------------------------------------------
gitlab-ci-gitlab          /assets/wrapper                  Up (healthy)   0.0.0.0:2221->22/tcp, 0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp
gitlab-ci-gitlab-runner   /usr/bin/dumb-init /entryp ...   Up
gitlab-ci-registry        /entrypoint.sh /etc/docker ...   Up             0.0.0.0:5000->5000/tcp
```

### Repository Access

The SSH port for Gitlab is remapped to port 2221. Gitlab needs to be configured
to later display the correct clone URL for the Git repositories.

In the configuration file `/etc/gitlab/gitlab.rb` in the container the following
line must be added:

```rb
gitlab_rails['gitlab_shell_ssh_port'] = 2221
```

The file can be edited via the Gitlab container:

```sh
docker exec -it gitlab-ci-gitlab vi etc/gitlab/gitlab.rb
```

The fastest way is to use `sed`:

```sh
docker exec -it gitlab-ci-gitlab sed -i "s/# gitlab_rails\['gitlab_shell_ssh_port'\] = 22/gitlab_rails\['gitlab_shell_ssh_port'\] = 2221/" /etc/gitlab/gitlab.rb
```

The effective configuration (without comments and empty lines) can be displayed
with

```sh
docker exec -it gitlab-ci-gitlab cat /etc/gitlab/gitlab.rb | grep -vP '^#|^\s$'
```

It is possible to map the HTTP port to something other than port 80. However,
that port has to be configured the compose file `docker-compose.yml` »inside«
and »outside« the same, e.g. `8080:8080` instead of `80:80`. In the
configuration file the option `external_url` has then to be set accordingly.

After changing the configuration the container has to be restarted:

```sh
docker restart gitlab-ci-gitlab
```

[runner-docker-executor]: https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnersdocker-section
